export type FiveDayThreeHourForecastItem = {
    dt_txt: string,
    main: {
        temp: number,
        temp_min: number,
        temp_max: number,
        humidity: number
    },
    weather: {
        icon: string
    }[]
};

export type FiveDayThreeHourForecastData = {
    list: FiveDayThreeHourForecastItem[]
}