import React from 'react';
import RefreshImage from "./refresh.svg";
import "./index.css";

interface Props {
    onClickButton: () => void
}

function RefreshButton(props: Props) {
    return (
        <button
            className="refreshButton"
            onClick={props.onClickButton}
        >
            <img src={RefreshImage} />
        </button>
    );
}

export default RefreshButton;