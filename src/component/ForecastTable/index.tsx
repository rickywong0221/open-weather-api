import React from 'react';
import Table from 'react-bootstrap/Table';
import {FiveDayThreeHourForecastItem} from "../../data/OpenWeatherData";


type Props = {
    list: FiveDayThreeHourForecastItem[]
}

function ForecastTable(props: Props) {
    return (
        <Table striped bordered hover variant="dark">
            <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Temperature (Min/Max)</th>
                <th>Humidity (%)</th>
                <th>Weather</th>
            </tr>
            </thead>
            <tbody>
            {
                props.list.map((item) => {
                    return (
                        <tr>
                            <td>{item.dt_txt.slice(0,10)}</td>
                            <td>{item.dt_txt.slice(11,16)}</td>
                            <td>{item.main.temp_min}/{item.main.temp_max}</td>
                            <td>{item.main.humidity}%</td>
                            <td>
                                <img
                                    src={`https://openweathermap.org/img/wn/${item.weather[0].icon}@2x.png`}
                                />
                            </td>
                        </tr>
                    );
                })
            }
            </tbody>
        </Table>
    );
}

export default ForecastTable;