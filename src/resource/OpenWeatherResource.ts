import axios from "axios";
import {FiveDayThreeHourForecastData} from "../data/OpenWeatherData";

export function get5Day3HourForecastData(callback: (data: FiveDayThreeHourForecastData | undefined) => void) {
    axios.get<FiveDayThreeHourForecastData>("https://api.openweathermap.org/data/2.5/forecast?id=1819730&units=metric&appid=ca0b98599b2780b148913f79a4d3ad4f")
        .then((response) => {
            if (response.status === 200) {
                callback(response.data);
            } else {
                callback(undefined);
            }
        })
        .catch((err) => {
            console.error(err);
            callback(undefined);
        })
}