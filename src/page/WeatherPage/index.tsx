import React from 'react';
import "./index.css";
import RefreshButton from "../../component/RefreshButton";
import ForecastTable from "../../component/ForecastTable";
import {FiveDayThreeHourForecastData} from "../../data/OpenWeatherData";
import {get5Day3HourForecastData} from "../../resource/OpenWeatherResource";
import WeatherChart, {ChartData} from "../../component/WeatherChart";


type Props = {};
type State = {
    forecastData: FiveDayThreeHourForecastData | undefined,
    lastUpdateTime: Date
};

export default class WeatherPage extends React.Component<Props, State>{
    constructor(props: Props) {
        super(props);
        this.state = {
            forecastData: undefined,
            lastUpdateTime: new Date()
        };
        this.onLoadedForecastData = this.onLoadedForecastData.bind(this);
        this.onClickRefreshButton = this.onClickRefreshButton.bind(this);
    }

    onLoadedForecastData(data: FiveDayThreeHourForecastData | undefined) {
        this.setState({
            forecastData: data
        });
    }

    onClickRefreshButton() {
        this.setState({
            lastUpdateTime: new Date()
        }, () => {
            get5Day3HourForecastData(this.onLoadedForecastData);
        });
    }

    toWeatherChartData(forecastData: FiveDayThreeHourForecastData | undefined) {
        if (!forecastData) {
            return [];
        }

        const weatherChartData: ChartData[] = forecastData.list.map((item) => {
            return {
                name: item.dt_txt,
                temperature: item.main.temp,
                humidity: item.main.humidity
            };
        });
        return weatherChartData;

    }

    componentDidMount() {
        get5Day3HourForecastData(this.onLoadedForecastData);
    }

    render() {
        const lastUpdateTime = this.state.lastUpdateTime;
        const date = lastUpdateTime.getFullYear() + '-' + (lastUpdateTime.getMonth()+1) + '-' + lastUpdateTime.getDate();
        const time = lastUpdateTime.getHours() + ":" + lastUpdateTime.getMinutes() + ":" + lastUpdateTime.getSeconds();
        return (
            <div className="weatherPage">
                <div className="left">
                    <h1>5 Day / 3 Hour Forecast</h1>
                    <h2>Hong Kong</h2>
                </div>
                <div className="right">
                    <div className="updateTime">Last Update Time: <br/>{date} {time}</div>
                    <RefreshButton
                        onClickButton={this.onClickRefreshButton}
                    />
                </div>
                <div className="chartContainer">
                    <WeatherChart
                        data={this.toWeatherChartData(this.state.forecastData)}
                    />
                </div>
                <ForecastTable
                    list={(this.state.forecastData) ? this.state.forecastData.list : []}
                />
            </div>
        );
    }

}



